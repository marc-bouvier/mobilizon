# Relay

Manages remote relays

## Make your instance follow a mobilizon instance

```bash
mix mobilizon.relay follow <relay_url>
```

Example: 
```bash
mix mobilizon.relay follow https://example.org/relay
```

## Make your instance unfollow a mobilizon instance

```bash
mix mobilizon.relay unfollow <relay_url>
```

Example: 
```bash
mix mobilizon.relay unfollow https://example.org/relay
```
